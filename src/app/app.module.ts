import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { NavbarComponent } from './navbar/navbar.component';

import { NgModule } from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {
  MatToolbarModule,
  MatTooltipModule,
  MatIconModule,
  MatSidenavModule,
  MatGridListModule,
  MatListModule, MatButtonModule, MatCardModule, MatMenuModule, MatTableModule, MatPaginatorModule, MatSortModule
} from '@angular/material';
import { LayoutModule } from '@angular/cdk/layout';
import { CardListComponent } from './card-list/card-list.component';
import { TableListComponent } from './table-list/table-list.component';

@NgModule({
  exports: [
    MatToolbarModule,
    MatTooltipModule,
    MatIconModule,
    MatSidenavModule,
    MatGridListModule,
    MatListModule
  ],
  declarations: [
    AppComponent,
    HeaderComponent,
    NavbarComponent,
    CardListComponent,
    TableListComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatGridListModule,
    MatCardModule,
    MatMenuModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule
  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
  bootstrap: [AppComponent],
  entryComponents: [],
  providers: []
})

export class AppModule { }
